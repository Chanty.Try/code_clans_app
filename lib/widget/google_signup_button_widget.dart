import 'package:flutter/material.dart';
import 'package:google_signin_example/provider/google_sign_in.dart';
import 'package:provider/provider.dart';

class GoogleSignupButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.all(4),
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.all(5),
          height: 50,
          decoration: BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: TextButton(
            onPressed: () {
              final provider =
                  Provider.of<GoogleSignInProvider>(context, listen: false);
              provider.login();
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "assets/images/google.png",
                  width: 20,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  "Sign up with Google",
                  style: TextStyle(color: Colors.white),
                ),
              ],
            ),
          ),
        ),
      );
}
