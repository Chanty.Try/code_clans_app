import 'package:flutter/material.dart';
import 'package:google_signin_example/widget/google_signup_button_widget.dart';

class SignUpWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.green,
        body: buildSignUp(),
      );

  Widget buildSignUp() => Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Spacer(),
            Image.asset(
              "assets/images/ic_logo.png",
              width: 60,
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              "Gocery",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 24),
            ),
            Spacer(),
            Container(
              padding: EdgeInsets.all(10),
              height: 160,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(20)),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        "Welcome ",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 24),
                      ),
                      Image.asset(
                        "assets/images/hi.png",
                        width: 25,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                      "If you already have Gocery account, enter email \nbelow"),
                  SizedBox(
                    height: 15,
                  ),
                  GoogleSignupButtonWidget()
                ],
              ),
            ),
          ],
        ),
      );
}
