import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_signin_example/provider/google_sign_in.dart';
import 'package:provider/provider.dart';

class LoggedInWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = FirebaseAuth.instance.currentUser;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          "Account",
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black),
        ),
      ),
      body: SafeArea(
          child: Column(
        children: [
          SizedBox(
            height: 20,
          ),
          CircleAvatar(
            maxRadius: 25,
            backgroundImage: NetworkImage(user.photoURL),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            user.displayName,
            //  user.displayName!,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          Text(user.email),
          SizedBox(
            height: 15,
          ),
          _buildProfileInfo(
              titles: 'Profile Settings',
              subtiles: 'Change Your Basic Profile'),
          _buildProfileInfo(titles: 'Promos', subtiles: 'Latest Promo from us'),
          _buildProfileInfo(titles: 'My Address', subtiles: 'Your Address'),
          _buildProfileInfo(
              titles: 'Terms, Privacy, & Policy',
              subtiles: 'Things You may want know'),
          _buildProfileInfo(
              titles: 'Help & Support', subtiles: 'Get support from us'),
          InkWell(
            onTap: () {
              // Navigator.pushReplacement(
              //   context,
              //   MaterialPageRoute(
              //     builder: (context) => SignUpPage(),
              //   ),
              // );
              Navigator.pop(context);
            },
            child: Container(
                padding: EdgeInsets.only(left: 20),
                width: double.infinity,
                child: InkWell(
                  onTap: () {
                    final provider = Provider.of<GoogleSignInProvider>(context,
                        listen: false);
                    provider.logout();
                  },
                  child: Text(
                    'Logout',
                    style: TextStyle(fontSize: 20),
                  ),
                )),
          )
        ],
      )),
    );
  }

  Widget _buildProfileInfo({String titles = '', String subtiles = ''}) {
    return Container(
      width: double.infinity,
      child: ListTile(
        title: Text(
          titles,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
        ),
        subtitle: Text(subtiles),
        trailing: Icon(
          Icons.arrow_forward_ios,
          color: Colors.green,
        ),
      ),
    );
  }
}
